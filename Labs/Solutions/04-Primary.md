### Hands-on Lab Solution: Dynamic Terraform and Modules

The primary assignment for this lab is divided into five sections. Each section involves specific tasks related to using dynamic blocks, conditional expressions, managing multiple instances, versioning modules, and creating composite modules in Terraform.

---

### Section 1: Implementing Dynamic Blocks in Resource Configurations

#### Primary Assignment

Implement dynamic blocks in a resource configuration. Complete the following tasks:

1. Define a variable to hold multiple ingress rules for a security group.
2. Use a dynamic block to create ingress rules based on the variable.
3. Apply the configuration and verify the dynamic block functionality.

### Step-by-Step Solution for Section 1

#### Step 1: Define a Variable for Ingress Rules

Add the following variable to the `variables.tf` file:

```hcl
# variables.tf

variable "ingress_rules" {
  description = "List of ingress rules for the security group"
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
  default = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}
```

#### Step 2: Create Dynamic Ingress Rules

Create a `security_group.tf` file with the following content to define the security group and dynamic ingress rules:

```hcl
# security_group.tf

resource "aws_security_group" "example" {
  name        = "example_sg"
  vpc_id      = aws_vpc.example.id
  description = "Example security group with dynamic ingress rules"

  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }
}
```

#### Step 3: Apply the Configuration

Apply the configuration to create the security group with dynamic ingress rules:

```sh
terraform apply
```

### Section 2: Utilizing Conditional Expressions to Create Flexible Configurations

#### Primary Assignment

Utilize conditional expressions to create flexible configurations. Complete the following tasks:

1. Define a variable to specify the environment (e.g., `dev`, `prod`).
2. Use conditional expressions to set resource properties based on the environment.
3. Apply the configuration and verify the conditional behavior.

### Step-by-Step Solution for Section 2

#### Step 1: Define a Variable for Environment

Add the following variable to the `variables.tf` file:

```hcl
# variables.tf

variable "environment" {
  description = "The environment for the resources (e.g., dev, prod)"
  type        = string
  default     = "dev"
}
```

#### Step 2: Use Conditional Expressions

Update `template.tf` to use conditional expressions:

```hcl
# template.tf

resource "aws_instance" "example" {
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = var.environment == "prod" ? "t2.small" : "t2.micro"
  subnet_id     = aws_subnet.example.id
  user_data     = data.template_file.user_data.rendered
}
```

#### Step 3: Apply the Configuration

Apply the configuration to create the instance with conditional properties:

```sh
terraform apply
```

### Section 3: Managing Multiple Instances of a Module or Resource with `count` and `for_each`

#### Primary Assignment

Manage multiple instances of a resource using `count` and `for_each`. Complete the following tasks:

1. Define variables to specify the number of instances and their types.
2. Use `count` to create multiple instances based on the variable.
3. Use `for_each` to create instances based on a map of instance types.
4. Apply the configuration and verify the instances.

### Step-by-Step Solution for Section 3

#### Step 1: Define Variables for Instance Management

Add the following variables to the `variables.tf` file:

```hcl
# variables.tf

variable "instance_count" {
  description = "The number of instances to create"
  type        = number
  default     = 2
}

variable "instance_types" {
  description = "Map of instance types with instance names"
  type = map(string)
  default = {
    instance1 = "t2.micro"
    instance2 = "t2.small"
  }
}
```

#### Step 2: Use `count` to Create Multiple Instances

Create a `instances_count.tf` file with the following content:

```hcl
# instances_count.tf

resource "aws_instance" "count_example" {
  count         = var.instance_count
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.example.id
}
```

#### Step 3: Use `for_each` to Create Instances Based on a Map

Create a `instances_for_each.tf` file with the following content:

```hcl
# instances_for_each.tf

resource "aws_instance" "for_each_example" {
  for_each      = var.instance_types
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = each.value
  subnet_id     = aws_subnet.example.id
}
```

#### Step 4: Apply the Configuration

Apply the configuration to create the instances with `count` and `for_each`:

```sh
terraform apply
```

### Section 4: Versioning and Releasing Modules

#### Primary Assignment

Implement versioning and release a module in Terraform. Complete the following tasks:

1. Define a module with version constraints.
2. Create a versioned module and use it in a configuration.
3. Apply the configuration and verify the module version.

### Step-by-Step Solution for Section 4

#### Step 1: Define a Module with Version Constraints

Create a new module directory `modules/example` with the following content:

```hcl
# modules/example/main.tf

variable "instance_type" {
  description = "The instance type for the EC2 instance"
  type        = string
  default     = "t2.micro"
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}

variable "subnet_id" {
  description = "The subnet ID for the instance"
  type        = string
}

resource "aws_instance" "example" {
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
}
```

#### Step 2: Create a Versioned Module

Add a `versions.tf` file to the module:

```hcl
# modules/example/versions.tf

terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }
  }
}
```

#### Step 3: Use the Versioned Module in a Configuration

Create a `modules.tf` file in the root configuration:

```hcl
# modules.tf

module "example" {
  source        = "./modules/example"
  instance_type = "t2.micro"
  ami_id        = data.aws_ami.latest_amazon_linux.id
  subnet_id     = aws_subnet.example.id
}
```

#### Step 4: Initialize and Apply the Configuration

Initialize and apply the configuration to use the versioned module:

```sh
terraform init
terraform apply
```

### Section 5: Creating and Managing Composite Modules

#### Primary Assignment

Create and manage composite modules in Terraform. Complete the following tasks:

1. Create multiple modules for different resources (e.g., network, compute).
2. Combine the modules into a single composite module.
3. Pass inputs and outputs between the combined modules.
4. Apply the configuration and verify the composite module functionality.

### Step-by-Step Solution for Section 5

#### Step 1: Create Multiple Modules

Create a `modules/network` directory with the following content:

```hcl
# modules/network/main.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-2a"
}
```

Create a `modules/compute` directory with the following content:

```hcl
# modules/compute/main.tf

variable "instance_type" {
  description = "The instance type for the EC2 instance"
  type        = string
  default     = "t2.micro"
}

variable "subnet_id" {
  description = "The subnet ID for the instance"
  type        = string
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}

resource "aws_instance" "example" {
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = var.sub

net_id
}
```

#### Step 2: Combine Modules into a Composite Module

Create a `modules/composite` directory with the following content:

```hcl
# modules/composite/main.tf

module "network" {
  source = "../network"
}

module "compute" {
  source        = "../compute"
  instance_type = "t2.micro"
  subnet_id     = module.network.subnet_id
  ami_id        = data.aws_ami.latest_amazon_linux.id
}
```

#### Step 3: Pass Inputs and Outputs Between Modules

Add the necessary outputs to the `network` module:

```hcl
# modules/network/outputs.tf

output "vpc_id" {
  value = aws_vpc.example.id
}

output "subnet_id" {
  value = aws_subnet.example.id
}
```

Update the `compute` module to accept the `subnet_id` as an input:

```hcl
# modules/compute/variables.tf

variable "subnet_id" {
  description = "The subnet ID for the instance"
  type        = string
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}
```

#### Step 4: Initialize and Apply the Configuration

Initialize and apply the configuration to verify the composite module functionality:

```sh
terraform init
terraform apply
```

### Complete File Contents

#### main.tf

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-XX"
  }
}
```

#### variables.tf

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}

variable "user_name" {
  description = "The name to include in the template"
  type        = string
  default     = "Terraform User"
}

variable "ingress_rules" {
  description = "List of ingress rules for the security group"
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
  default = [
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    },
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}

variable "environment" {
  description = "The environment for the resources (e.g., dev, prod)"
  type        = string
  default     = "dev"
}

variable "instance_count" {
  description = "The number of instances to create"
  type        = number
  default     = 2
}

variable "instance_types" {
  description = "Map of instance types with instance names"
  type = map(string)
  default = {
    instance1 = "t2.micro"
    instance2 = "t2.small"
  }
}

variable "server_name" {
  description = "The name of the server"
  type        = string
  default     = "default_server"
}

variable "server_ip" {
  description = "The IP address of the server"
  type        = string
  default     = "192.168.1.1"
}

variable "server_count" {
  description = "The number of servers"
  type        = number
  default     = 1
}
```

#### vpc.tf

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-2a"
}
```

#### dynamodb.tf

```hcl
# dynamodb.tf

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = var.dynamodb_billing_mode

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

#### security_group.tf

```hcl
# security_group.tf

resource "aws_security_group" "example" {
  name        = "example_sg"
  vpc_id      = aws_vpc.example.id
  description = "Example security group with dynamic ingress rules"

  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }
}
```

#### template.tf

```hcl
# template.tf

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    user_name = data.external.custom_data.result.user_name
  }
}

data "template_file" "complex" {
  template = file("${path.module}/complex_template.tpl")
  vars = {
    server_name  = var.server_name,
    server_ip    = var.server_ip,
    server_count = var.server_count
  }
}

resource "aws_instance" "example" {
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = var.environment == "prod" ? "t2.small" : "t2.micro"
  subnet_id     = aws_subnet.example.id
  user_data     = data.template_file.user_data.rendered
}
```

#### instances_count.tf

```hcl
# instances_count.tf

resource "aws_instance" "count_example" {
  count         = var.instance_count
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.example.id
}
```

#### instances_for_each.tf

```hcl
# instances_for_each.tf

resource "aws_instance" "for_each_example" {
  for_each      = var.instance_types
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = each.value
  subnet_id     = aws_subnet.example.id
}
```

#### modules.tf

```hcl
# modules.tf

module "example" {
  source        = "./modules/example"
  instance_type = "t2.micro"
  ami_id        = data.aws_ami.latest_amazon_linux.id
  subnet_id     = aws_subnet.example.id
}
```

#### outputs.tf

```hcl
# outputs.tf

output "rendered_template" {
  value = data.template_file.user_data.rendered
}

output "rendered_complex_template" {
  value = data.template_file.complex.rendered
}
```

#### datasources.tf

```hcl
# datasources.tf

data "aws_ami" "latest_amazon_linux" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
```

#### external.tf

```hcl
# external.tf

data "external" "custom_data" {
  program = ["${path.module}/fetch_external_data.sh"]
}
```

#### user_data.tpl

```hcl
# user_data.tpl

#!/bin/bash
echo "Hello, ${user_name}! Welcome to your new instance." > /home/ec2-user/welcome.txt
```

#### complex_template.tpl

```hcl
# complex_template.tpl

%{ if server_name != "" }
server {
  name = "${server_name}"
  ip   = "${server_ip}"
}
%{ endif }

%{ if server_count > 1 }
multiple_servers = true
%{ else }
multiple_servers = false
%{ endif }
```

#### fetch_external_data.sh

```sh
#!/bin/bash
echo '{"user_name": "External User"}'
```

#### modules/example/main.tf

```hcl
# modules/example/main.tf

variable "instance_type" {
  description = "The instance type for the EC2 instance"
  type        = string
  default     = "t2.micro"
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}

variable "subnet_id" {
  description =

 "The subnet ID for the instance"
  type        = string
}

resource "aws_instance" "example" {
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
}
```

#### modules/example/versions.tf

```hcl
# modules/example/versions.tf

terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }
  }
}
```

#### modules/network/main.tf

```hcl
# modules/network/main.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-2a"
}
```

#### modules/network/outputs.tf

```hcl
# modules/network/outputs.tf

output "vpc_id" {
  value = aws_vpc.example.id
}

output "subnet_id" {
  value = aws_subnet.example.id
}
```

#### modules/compute/main.tf

```hcl
# modules/compute/main.tf

variable "instance_type" {
  description = "The instance type for the EC2 instance"
  type        = string
  default     = "t2.micro"
}

variable "subnet_id" {
  description = "The subnet ID for the instance"
  type        = string
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}

resource "aws_instance" "example" {
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
}
```

#### modules/compute/variables.tf

```hcl
# modules/compute/variables.tf

variable "subnet_id" {
  description = "The subnet ID for the instance"
  type        = string
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}
```

#### modules/composite/main.tf

```hcl
# modules/composite/main.tf

module "network" {
  source = "../network"
}

module "compute" {
  source        = "../compute"
  instance_type = "t2.micro"
  subnet_id     = module.network.subnet_id
  ami_id        = data.aws_ami.latest_amazon_linux.id
}
```

### Summary

You have successfully completed the primary assignments for the "Hands-on Lab: Dynamic Terraform and Modules," including implementing dynamic blocks, utilizing conditional expressions, managing multiple instances with `count` and `for_each`, versioning modules, and creating composite modules. This comprehensive approach ensures that you are proficient in using advanced Terraform features and module management.
