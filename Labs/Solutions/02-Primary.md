### Hands-on Lab Solution: Variables and Locals

#### Primary Assignment

The primary assignment for this lab is divided into three sections. Each section involves specific tasks related to variables and locals in Terraform.

---

### Section 1: Exploring Terraform Expressions with Terraform Console

#### Primary Assignment

Use the Terraform console to explore and evaluate various Terraform expressions. Perform the following tasks:

1. Create and migrate values to variables. (`var.vpc_cidr_block` & `var.dynamodb_billing_mode`)
2. Launch the Terraform console from your lab machine.
3. Evaluate the values of existing variables and resources.
4. Test conditional expressions and functions.
   - Test a simple conditional expression
   - Test the length function
   - Test the coalesce function
   - Test a custom function to count the number of subnets

### Step-by-Step Solution for Section 1

#### Step 1: Create and Migrate Values to Variables

Create a `variables.tf` file with the following content:

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
}
```

Update the `vpc.tf` file to use the `vpc_cidr_block` variable:

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}
```

Update the `dynamodb.tf` file to use the `dynamodb_billing_mode` variable:

```hcl
# dynamodb.tf

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = var.dynamodb_billing_mode

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

Verify that the changes do not affect the existing infrastructure:

```sh
terraform plan
```

#### Step 2: Launch the Terraform Console from Your Lab Machine

Launch the Terraform console:

```sh
terraform console
```

#### Step 3: Evaluate the Values of Existing Variables and Resources

In the Terraform console, you can evaluate the values of the variables and resources:

```hcl
# Evaluate the value of the vpc_cidr_block variable
> var.vpc_cidr_block
"10.0.0.0/16"

# Evaluate the value of the dynamodb_billing_mode variable
> var.dynamodb_billing_mode
"PAY_PER_REQUEST"

# Evaluate the ID of the created VPC
> aws_vpc.example.id
```

#### Step 4: Test Conditional Expressions and Functions

Test various conditional expressions and functions in the Terraform console:

```hcl
# Test a simple conditional expression
> var.vpc_cidr_block == "10.0.0.0/16" ? "CIDR block is 10.0.0.0/16" : "CIDR block is different"
"CIDR block is 10.0.0.0/16"

# Test the length function
> length(var.vpc_cidr_block)
11

# Test the coalesce function
> coalesce("", "default", "fallback")
"default"

# Test a custom function to count the number of subnets
> length([for subnet in ["10.0.1.0/24", "10.0.2.0/24"]: subnet])
2
```

---

### Section 2: Utilize Various Variable Input Options

#### Primary Assignment

Utilize various variable input options to configure a Terraform project. Complete the following tasks:

1. Define variables in the configuration file.
2. Override variables using CLI arguments, environment variables, and variable files.
3. Apply the configuration and verify the variable values.

### Step-by-Step Solution for Section 2

#### Step 1: Define Variables in the Configuration File

Ensure that `variables.tf` contains the following variables:

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
}
```

#### Step 2: Override Variables Using CLI Arguments, Environment Variables, and Variable Files

##### Using CLI Arguments

Override the `vpc_cidr_block` variable using the CLI argument:

```sh
terraform apply -var="vpc_cidr_block=10.1.0.0/16"
```

##### Using Environment Variables

Set an environment variable to override the `vpc_cidr_block` variable:

```sh
export TF_VAR_vpc_cidr_block="10.2.0.0/16"
terraform apply
```

##### Using Variable Files

Create a `terraform.tfvars` file with the following content:

```hcl
# terraform.tfvars

vpc_cidr_block = "10.3.0.0/16"
```

Apply the configuration using the variable file:

```sh
terraform apply -var-file="terraform.tfvars"
```

#### Step 3: Apply the Configuration and Verify the Variable Values

Verify that the variables are applied correctly by running the configuration and checking the output:

```sh
terraform plan
terraform apply
```

---

### Section 3: Test Variables with Validation

#### Primary Assignment

Implement variable validation in a Terraform project. Complete the following tasks:

1. Define variables with validation rules.
2. Test the validation rules by providing different input values.
3. Apply the configuration and observe the validation behavior.

### Step-by-Step Solution for Section 3

#### Step 1: Define Variables with Validation Rules

Update `variables.tf` to include validation rules:

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}
```

#### Step 2: Test the Validation Rules by Providing Different Input Values

Test with valid and invalid values for the `vpc_cidr_block` variable:

```sh
# Test with valid values
terraform apply -var="vpc_cidr_block=10.1.0.0/16"

# Test with invalid values
terraform apply -var="vpc_cidr_block=invalid-cidr"
```

#### Step 3: Apply the Configuration and Observe the Validation Behavior

Apply the configuration and observe how Terraform responds to the valid and invalid inputs:

```sh
terraform apply
```

### Complete File Contents

#### main.tf

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-XX"
  }
}
```

#### variables.tf

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}
```

#### dynamodb.tf

```hcl
# dynamodb.tf

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = var.dynamodb_billing_mode

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

#### vpc.tf

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}
```

### Summary

You have successfully completed the primary assignments for the "Hands-on Lab: Variables and Locals," including exploring Terraform expressions with the Terraform console, utilizing various variable input options, and testing variables with validation. This comprehensive approach ensures that you are proficient in managing and validating variables within your Terraform projects.
