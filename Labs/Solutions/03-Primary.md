### Hands-on Lab Solution: Terraform Templates

#### Primary Assignment

The primary assignment for this lab is divided into four sections. Each section involves specific tasks related to using templates in Terraform.

---

### Section 1: Setting up Templates in Terraform

#### Primary Assignment

Set up templates in Terraform to generate dynamic resource configurations. Complete the following tasks:

1. Create a template file that will be used to add a file added to an AWS instance.
2. Use the `templatefile` function to include that file from the template with variable values in an `aws_instance`.
3. Output the rendered template content and verify its correctness.

### Step-by-Step Solution for Section 1

#### Step 1: Create a Template File

Create a template file named `user_data.tpl` with the following content:

```hcl
# user_data.tpl

#!/bin/bash
echo "Hello, ${user_name}! Welcome to your new instance." > /home/ec2-user/welcome.txt
```

#### Step 2: Define the Variable in `variables.tf`

Update the `variables.tf` file to include the variable `user_name`:

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}

variable "user_name" {
  description = "The name to include in the template"
  type        = string
  default     = "Terraform User"
}
```

#### Step 3: Create the VPC and Subnet

Create a `vpc.tf` file to define the VPC and Subnet:

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-2a"
}
```

#### Step 4: Use the `templatefile` Function

Create a `template.tf` file with the following content to use the `templatefile` function:

```hcl
# template.tf

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    user_name = var.user_name
  }
}

resource "aws_instance" "example" {
  ami           = "ami-0508ce435f69fcedb"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.example.id
  user_data     = data.template_file.user_data.rendered
}
```

#### Step 5: Output the Rendered Template Content

Create an `outputs.tf` file to output the rendered template content:

```hcl
# outputs.tf

output "rendered_template" {
  value = data.template_file.user_data.rendered
}
```

Apply the configuration and verify the output:

```sh
terraform init
terraform apply
```

### Section 2: Mapping Templates with Various Data Sources

#### Primary Assignment

Map templates with various data sources in Terraform. Complete the following tasks:

1. Create a data source to retrieve the latest AWS AMI.
2. Use the retrieved data in a template to configure an AWS instance.
3. Render the template and apply the configuration.

### Step-by-Step Solution for Section 2

#### Step 1: Create a Data Source

Create a `datasources.tf` file to retrieve the latest AWS AMI:

```hcl
# datasources.tf

data "aws_ami" "latest_amazon_linux" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
```

#### Step 2: Use the Retrieved Data in a Template

Update `template.tf` to use the retrieved AMI data:

```hcl
# template.tf

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    user_name = var.user_name
  }
}

resource "aws_instance" "example" {
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.example.id
  user_data     = data.template_file.user_data.rendered
}
```

#### Step 3: Render the Template and Apply the Configuration

Apply the configuration:

```sh
terraform apply
```

### Section 3: Utilizing External Data Sources

#### Primary Assignment

Utilize external data sources in Terraform. Complete the following tasks:

1. Create a custom script to fetch external data.
2. Define an external data source in Terraform using the custom script.
3. Use the retrieved data in a template to generate resource configurations.

### Step-by-Step Solution for Section 3

#### Step 1: Create a Custom Script

Create a script named `fetch_external_data.sh` with the following content:

```sh
#!/bin/bash
echo '{"user_name": "External User"}'
```

Make the script executable:

```sh
chmod +x fetch_external_data.sh
```

#### Step 2: Define an External Data Source

Create a `external.tf` file to define the external data source:

```hcl
# external.tf

data "external" "custom_data" {
  program = ["${path.module}/fetch_external_data.sh"]
}
```

#### Step 3: Use the Retrieved Data in a Template

Update `template.tf` to use the external data source:

```hcl
# template.tf

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    user_name = data.external.custom_data.result.user_name
  }
}

resource "aws_instance" "example" {
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.example.id
  user_data     = data.template_file.user_data.rendered
}
```

Reinitialize Terraform to add the `hashicorp/external` provider:

```sh
terraform init
```

Apply the configuration:

```sh
terraform apply
```

### Section 4: Building and Configuring Template Files

#### Primary Assignment

Build and configure template files in Terraform. Complete the following tasks:

1. Create a complex template file with variables, conditionals, and loops.
2. Define the necessary variables in the main configuration.
3. Render the template and verify the generated content.

### Step-by-Step Solution for Section 4

#### Step 1: Create a Complex Template File

Create a template file named `complex_template.tpl` with the following content:

```hcl
# complex_template.tpl

%{ if server_name != "" }
server {
  name = "${server_name}"
  ip   = "${server_ip}"
}
%{ endif }

%{ if server_count > 1 }
multiple_servers = true
%{ else }
multiple_servers = false
%{ endif }
```

#### Step 2: Define the Necessary Variables

Update `variables.tf` to include the necessary variables:

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}

variable "user_name" {
  description = "The name to include in the template"
  type        = string
  default     = "Terraform User"
}

variable "server_name" {
  description = "The name of the server"
  type        = string
  default     = "default_server"
}

variable "server_ip" {
  description = "The IP address of the server"
  type        = string
  default     = "192.168.1.1"
}

variable "server_count" {
  description = "The number of servers"
  type        = number
  default     = 1
}
```

#### Step 3: Render the Template and Verify the Generated Content

Update `template.tf` to use the complex template file and keep `data.template_file.user_data`

 using the external data source from Section 3:

```hcl
# template.tf

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    user_name = data.external.custom_data.result.user_name
  }
}

data "template_file" "complex" {
  template = file("${path.module}/complex_template.tpl")
  vars = {
    server_name  = var.server_name,
    server_ip    = var.server_ip,
    server_count = var.server_count
  }
}

resource "aws_instance" "example" {
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.example.id
  user_data     = data.template_file.user_data.rendered
}
```

#### Step 4: Output the Rendered Complex Template Content

Update the `outputs.tf` file to output the rendered complex template content:

```hcl
# outputs.tf

output "rendered_template" {
  value = data.template_file.user_data.rendered
}

output "rendered_complex_template" {
  value = data.template_file.complex.rendered
}
```

Apply the configuration and verify the output:

```sh
terraform apply
```

### Complete File Contents

#### main.tf

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-XX"
  }
}
```

#### variables.tf

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}

variable "user_name" {
  description = "The name to include in the template"
  type        = string
  default     = "Terraform User"
}

variable "server_name" {
  description = "The name of the server"
  type        = string
  default     = "default_server"
}

variable "server_ip" {
  description = "The IP address of the server"
  type        = string
  default     = "192.168.1.1"
}

variable "server_count" {
  description = "The number of servers"
  type        = number
  default     = 1
}
```

#### vpc.tf

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-2a"
}
```

#### dynamodb.tf

```hcl
# dynamodb.tf

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = var.dynamodb_billing_mode

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

#### template.tf

```hcl
# template.tf

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    user_name = data.external.custom_data.result.user_name
  }
}

data "template_file" "complex" {
  template = file("${path.module}/complex_template.tpl")
  vars = {
    server_name  = var.server_name,
    server_ip    = var.server_ip,
    server_count = var.server_count
  }
}

resource "aws_instance" "example" {
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.example.id
  user_data     = data.template_file.user_data.rendered
}
```

#### outputs.tf

```hcl
# outputs.tf

output "rendered_template" {
  value = data.template_file.user_data.rendered
}

output "rendered_complex_template" {
  value = data.template_file.complex.rendered
}
```

#### datasources.tf

```hcl
# datasources.tf

data "aws_ami" "latest_amazon_linux" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
```

#### external.tf

```hcl
# external.tf

data "external" "custom_data" {
  program = ["${path.module}/fetch_external_data.sh"]
}
```

#### user_data.tpl

```hcl
# user_data.tpl

#!/bin/bash
echo "Hello, ${user_name}! Welcome to your new instance." > /home/ec2-user/welcome.txt
```

#### complex_template.tpl

```hcl
# complex_template.tpl

%{ if server_name != "" }
server {
  name = "${server_name}"
  ip   = "${server_ip}"
}
%{ endif }

%{ if server_count > 1 }
multiple_servers = true
%{ else }
multiple_servers = false
%{ endif }
```

#### fetch_external_data.sh

```sh
#!/bin/bash
echo '{"user_name": "External User"}'
```

### Summary

You have successfully completed the primary assignments for the "Hands-on Lab: Terraform Templates," including setting up templates, mapping templates with various data sources, utilizing external data sources, and building and configuring complex template files. This comprehensive approach ensures that you are proficient in using templates within your Terraform projects.
