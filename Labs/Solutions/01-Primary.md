### Hands-on Lab Solution: Setting Up and Managing State in Terraform

#### Primary Assignment

Your primary assignment is to set up a remote state backend for Terraform using AWS S3 and DynamoDB for state locking. Follow these steps:

1. **Use the provided S3 bucket for state storage:**
   - Bucket name: `cprimelearning-tflabs-XX`

2. **Create a DynamoDB table for state locking:**
   - Table name: `terraform-state-lock-XX`

3. **Configure your Terraform project to use the remote state backend.**
4. **Apply your configuration to verify that the state is stored remotely and that locking is enabled.**

### Step-by-Step Solution

#### Step 1: Initialize the Terraform Project with Local State

Create a `main.tf` file with the following content to define the provider:

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}
```

#### Step 2: Create the DynamoDB Table for State Locking

Create a file named `dynamodb.tf` with the following content to define the DynamoDB table:

> _Note: Replace  the `XX` in `terraform-state-lock-XX` with your sequence number._

```hcl
# dynamodb.tf

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

Initialize Terraform and apply the configuration to create the DynamoDB table:

```sh
terraform init
terraform apply
```

#### Step 3: Configure Terraform to Use the Remote State Backend

Modify the `main.tf` file to include the remote state backend configuration:

> _Note: Replace the `XX` in `cprimelearning-tflabs-XX` and `terraform-state-lock-XX` with your sequence number._

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-XX"
  }
}
```

Re-initialize Terraform to configure the remote backend:

```sh
terraform init
```

You should see output indicating that Terraform is configuring the backend and migrating the state to S3.

#### Step 4: Create the VPC Resource

Create a `vpc.tf` file with the following content to define the VPC resource:

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
}
```

#### Step 5: Apply Your Configuration

Apply the configuration to create the VPC:

```sh
terraform apply
```

#### Step 6: Verify Remote State and Locking

Check the S3 bucket (`cprimelearning-tflabs-XX`) to ensure that the Terraform state file is stored there. You should see a file named `terraform/state/terraform.tfstate`.

Also, verify that the DynamoDB table (`terraform-state-lock-XX`) is being used for state locking. You can check the DynamoDB table to see if there are any lock records created during the apply process.

### Complete File Contents

> _Note: Replace the `XX` in `cprimelearning-tflabs-XX` and `terraform-state-lock-XX` with your sequence number._

#### main.tf

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-XX"
  }
}
```

#### dynamodb.tf

```hcl
# dynamodb.tf

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

#### vpc.tf

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
}
```

### Summary

You have successfully set up a remote state backend for Terraform using AWS S3 for state storage and DynamoDB for state locking. This configuration ensures that your Terraform state is securely stored and that concurrent operations are managed effectively through state locking. You also created a VPC resource to verify the remote state and locking functionality.
