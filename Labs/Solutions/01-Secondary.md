### Optional Secondary Assignments for "Hands-on Lab: Setting Up and Managing State in Terraform"

#### Optional Secondary Assignments

1. **Import Existing Resources:**
   - **Objective:** Import an existing AWS resource (e.g., an EC2 instance) into Terraform.
   - **Steps:**
     1. Identify an existing AWS resource, such as an EC2 instance.
     2. Use the `terraform import` command to import the resource into Terraform.
     3. Write the corresponding Terraform configuration for the imported resource.

   **Example Steps:**
   ```sh
   # Step 1: Identify the EC2 instance ID (e.g., i-0123456789abcdef0)

   # Step 2: Import the EC2 instance into Terraform
   terraform import aws_instance.example i-0123456789abcdef0

   # Step 3: Write the Terraform configuration for the imported resource
   # Create a file named ec2.tf with the following content:
   resource "aws_instance" "example" {
     ami           = "ami-0abcdef1234567890"
     instance_type = "t2.micro"
     # Add other necessary configurations based on the imported resource
   }
   ```

2. **State Integrity Check:**
   - **Objective:** Implement checksums to ensure the integrity of the state file stored in S3.
   - **Steps:**
     1. Compute the checksum of the state file after each apply.
     2. Configure a mechanism to verify the checksum before applying changes to ensure that the state file has not been tampered with.
     3. Create a script that automates the verification process and integrates it into your Terraform workflow.

   **Example Steps:**
   ```sh
   # Step 1: Compute the checksum of the state file
   aws s3api head-object --bucket cprimelearning-tflabs-XX --key terraform/state --query ETag --output text > state_checksum.txt

   # Step 2: Create a script to verify the checksum
   # Create a file named verify_checksum.sh with the following content:
   stored_checksum=$(aws s3api head-object --bucket cprimelearning-tflabs-XX --key terraform/state --query ETag --output text)
   local_checksum=$(cat state_checksum.txt)

   if [ "$stored_checksum" == "$local_checksum" ]; then
     echo "State file integrity verified."
   else
     echo "State file integrity check failed."
     exit 1
   fi

   # Step 3: Integrate the script into your Terraform workflow
   ./verify_checksum.sh
   terraform apply -auto-approve
   ```

3. **State Snapshot Management:**
   - **Objective:** Create a mechanism to take periodic snapshots of your Terraform state file.
   - **Steps:**
     1. Create a script to take state snapshots.
     2. Store these snapshots in a secure location separate from your primary state file.
     3. Develop a procedure to restore the state from a snapshot in case of state file corruption or loss.

   **Example Steps:**
   ```sh
   # Step 1: Create a script to take state snapshots
   # Create a file named snapshot_state.sh with the following content:
   timestamp=$(date +%Y%m%d%H%M%S)
   aws s3 cp s3://cprimelearning-tflabs-XX/terraform/state s3://cprimelearning-tflabs-XX/terraform/snapshots/state-$timestamp.tfstate

   # Step 2: Schedule the script to run periodically using a cron job
   crontab -e
   # Add the following line to run the script daily at midnight
   0 0 * * * /path/to/snapshot_state.sh

   # Step 3: Restore from a snapshot if needed
   aws s3 cp s3://cprimelearning-tflabs-XX/terraform/snapshots/state-<timestamp>.tfstate terraform.tfstate
   terraform apply -state=terraform.tfstate -auto-approve
   ```
