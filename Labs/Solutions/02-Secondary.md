### Hands-on Lab Solution: Variables and Locals

#### Primary Assignment

The primary assignment for this lab is divided into three sections. Each section involves specific tasks related to variables and locals in Terraform.

---

### Section 1: Exploring Terraform Expressions with Terraform Console

#### Primary Assignment

Use the Terraform console to explore and evaluate various Terraform expressions. Perform the following tasks:

1. Create and migrate values to variables. (`var.vpc_cidr_block` & `var.dynamodb_billing_mode`)
2. Launch the Terraform console from your lab machine.
3. Evaluate the values of existing variables and resources.
4. Test conditional expressions and functions.
   - Test a simple conditional expression
   - Test the length function
   - Test the coalesce function
   - Test a custom function to count the number of subnets

### Step-by-Step Solution for Section 1

#### Step 1: Create and Migrate Values to Variables

Create a `variables.tf` file with the following content:

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
}
```

Update the `vpc.tf` file to use the `vpc_cidr_block` variable:

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}
```

Update the `dynamodb.tf` file to use the `dynamodb_billing_mode` variable:

```hcl
# dynamodb.tf

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = var.dynamodb_billing_mode

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

Verify that the changes do not affect the existing infrastructure:

```sh
terraform plan
```

#### Step 2: Launch the Terraform Console from Your Lab Machine

Launch the Terraform console:

```sh
terraform console
```

#### Step 3: Evaluate the Values of Existing Variables and Resources

In the Terraform console, you can evaluate the values of the variables and resources:

```hcl
# Evaluate the value of the vpc_cidr_block variable
> var.vpc_cidr_block
"10.0.0.0/16"

# Evaluate the value of the dynamodb_billing_mode variable
> var.dynamodb_billing_mode
"PAY_PER_REQUEST"

# Evaluate the ID of the created VPC
> aws_vpc.example.id
```

#### Step 4: Test Conditional Expressions and Functions

Test various conditional expressions and functions in the Terraform console:

```hcl
# Test a simple conditional expression
> var.vpc_cidr_block == "10.0.0.0/16" ? "CIDR block is 10.0.0.0/16" : "CIDR block is different"
"CIDR block is 10.0.0.0/16"

# Test the length function
> length(var.vpc_cidr_block)
11

# Test the coalesce function
> coalesce("", "default", "fallback")
"default"

# Test a custom function to count the number of subnets
> length([for subnet in ["10.0.1.0/24", "10.0.2.0/24"]: subnet])
2
```

### Optional Secondary Assignments for Section 1

1. **Complex Expressions:**
   - **Create and evaluate complex expressions involving multiple functions and conditionals.**
   - **Document the evaluated results and explain each step.**

   **Example Steps:**
   ```hcl
   # Launch the Terraform console
   terraform console

   # Create and evaluate a complex expression
   > coalesce(length(var.vpc_cidr_block) > 10 ? "Valid CIDR" : "", "Invalid CIDR", "Error")
   "Valid CIDR"

   # Document the evaluated results and explain each step
   # coalesce function checks if the length of vpc_cidr_block is greater than 10, if true, it returns "Valid CIDR", otherwise it returns the next non-empty value.
   ```

2. **Custom Functions:**
   - **Write and test custom functions using the Terraform console.**
   - **Explore the capabilities of built-in functions like `join`, `split`, and `lookup`.**

   **Example Steps:**
   ```hcl
   # Launch the Terraform console
   terraform console

   # Write and evaluate custom functions
   # Example using join
   > join(", ", ["subnet1", "subnet2", "subnet3"])
   "subnet1, subnet2, subnet3"

   # Example using split
   > split(", ", "subnet1, subnet2, subnet3")
   [
     "subnet1",
     "subnet2",
     "subnet3",
   ]

   # Example using lookup
   > lookup({a="apple", b="banana", c="cherry"}, "b", "unknown")
   "banana"
   ```

3. **Debugging:**
   - **Use the Terraform console to debug a configuration with syntax errors.**
   - **Identify and fix the errors based on the console output.**

   **Example Steps:**
   ```hcl
   # Introduce a syntax error in vpc.tf
   resource "aws_vpc" "example" {
     cidr_block = var.vpc_cidr_block
     invalid_syntax = "missing quote
   }

   # Launch the Terraform console
   terraform console

   # Attempt to evaluate the faulty configuration
   > aws_vpc.example.cidr_block
   # Error output indicating the issue with the syntax

   # Fix the syntax error and re-evaluate
   # Correct vpc.tf
   resource "aws_vpc" "example" {
     cidr_block = var.vpc_cidr_block
     invalid_syntax = "missing quote"
   }
   ```

---

### Section 2: Utilize Various Variable Input Options

#### Primary Assignment

Utilize various variable input options to configure a Terraform project. Complete the following tasks:

1. Define variables in the configuration file.
2. Override variables using CLI arguments, environment variables, and variable files.
3. Apply the configuration and verify the variable values.

### Step-by-Step Solution for Section 2

#### Step 1: Define Variables in the Configuration File

Ensure that `variables.tf` contains the following variables:

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
}
```

#### Step 2: Override Variables Using CLI Arguments, Environment Variables, and Variable Files

##### Using CLI Arguments

Override the `vpc_cidr_block` variable using the CLI argument:

```sh
terraform apply -var="vpc_cidr_block=10.1.0.0/16"
```

##### Using Environment Variables

Set an environment variable to override the `vpc_cidr_block` variable:

```sh
export TF_VAR_vpc_cidr_block="10.2.0.0/16"
terraform apply
```

##### Using Variable Files

Create a `terraform.tfvars` file with the following content:

```hcl
# terraform.tfvars

vpc_cidr_block = "10.3.0.0/16"
```

Apply the configuration using the variable file:

```sh
terraform apply -var-file="terraform.tfvars"
```

#### Step 3: Apply the Configuration and Verify the Variable Values

Verify that the variables are applied correctly by running the configuration and checking the output:

```sh
terraform plan
terraform apply
```

### Optional Secondary Assignments for Section 2

1. **Variable Precedence:**
   - **Experiment with variable precedence by setting the same variable using different methods.**
   - **Document which method takes precedence in various scenarios.**

   **Example Steps:**
   ```sh
   # Define a variable in the configuration file
   echo 'variable "vpc_cidr_block" { default = "10.0.0.0/16" }' >> variables.tf

   # Set the variable using an environment variable
   export TF_VAR_vpc_cidr_block="10.1.0.0/16"

   # Override the variable using a CLI argument
   terraform plan -var="vpc_cidr_block=10.2.0.0/16"

   # Document the precedence order observed
   # CLI argument > Environment variable > Default value in configuration file
   ```

2. **Sensitive Variables:**
   - **Mark variables as sensitive and observe how Terraform handles them.**
   - **Test different ways to securely pass sensitive variables.**

   **Example Steps:**
   ```hcl
   # Mark a variable as sensitive in variables.tf
   variable "db_password" {
     description = "The database password"
     type        = string
     sensitive   = true
   }

   # Set the sensitive variable using an environment variable
   export TF_VAR_db_password="my-secret-password"

   # Apply the configuration
   terraform apply

   # Observe how

 Terraform masks the sensitive variable value in the output
   ```

3. **Default Values:**
   - **Define default values for variables and override them.**
   - **Test how default values behave when no override is provided.**

   **Example Steps:**
   ```hcl
   # Define a variable with a default value in variables.tf
   variable "instance_type" {
     description = "The instance type for the EC2 instance"
     type        = string
     default     = "t2.micro"
   }

   # Apply the configuration without overriding the default value
   terraform apply

   # Override the default value using a CLI argument
   terraform apply -var="instance_type=t2.large"

   # Document the behavior observed
   ```

---

### Section 3: Test Variables with Validation

#### Primary Assignment

Implement variable validation in a Terraform project. Complete the following tasks:

1. Define variables with validation rules.
2. Test the validation rules by providing different input values.
3. Apply the configuration and observe the validation behavior.

### Step-by-Step Solution for Section 3

#### Step 1: Define Variables with Validation Rules

Update `variables.tf` to include validation rules:

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}
```

#### Step 2: Test the Validation Rules by Providing Different Input Values

Test with valid and invalid values for the `vpc_cidr_block` variable:

```sh
# Test with valid values
terraform apply -var="vpc_cidr_block=10.1.0.0/16"

# Test with invalid values
terraform apply -var="vpc_cidr_block=invalid-cidr"
```

#### Step 3: Apply the Configuration and Observe the Validation Behavior

Apply the configuration and observe how Terraform responds to the valid and invalid inputs:

```sh
terraform apply
```

### Optional Secondary Assignments for Section 3

1. **Complex Validation:**
   - **Implement complex validation rules involving multiple conditions.**
   - **Document the validation logic and test cases.**

   **Example Steps:**
   ```hcl
   # Define a variable with complex validation rules in variables.tf
   variable "instance_count" {
     description = "Number of instances to create"
     type        = number
     validation {
       condition     = var.instance_count > 0 && var.instance_count <= 10
       error_message = "The instance_count must be between 1 and 10."
     }
   }

   # Test the validation rules with valid and invalid values
   terraform apply -var="instance_count=5"   # Valid
   terraform apply -var="instance_count=15"  # Invalid
   ```

2. **Custom Error Messages:**
   - **Define custom error messages for validation failures.**
   - **Test the error messages to ensure they provide clear guidance.**

   **Example Steps:**
   ```hcl
   # Update the validation rule to include a custom error message
   variable "instance_type" {
     description = "The instance type for the EC2 instance"
     type        = string
     default     = "t2.micro"
     validation {
       condition     = contains(["t2.micro", "t2.large"], var.instance_type)
       error_message = "The instance_type must be either 't2.micro' or 't2.large'."
     }
   }

   # Test the validation rule with valid and invalid values
   terraform apply -var="instance_type=t2.micro"  # Valid
   terraform apply -var="instance_type=t2.small"  # Invalid, triggers custom error message
   ```

3. **Cross-variable Validation:**
   - **Implement validation rules that depend on the values of other variables.**
   - **Test scenarios where cross-variable dependencies are involved.**

   **Example Steps:**
   ```hcl
   # Define variables with cross-variable validation rules
   variable "min_size" {
     description = "Minimum size of the autoscaling group"
     type        = number
     default     = 1
   }

   variable "max_size" {
     description = "Maximum size of the autoscaling group"
     type        = number
     default     = 5
     validation {
       condition     = var.max_size > var.min_size
       error_message = "The max_size must be greater than min_size."
     }
   }

   # Test the cross-variable validation rule with valid and invalid values
   terraform apply -var="min_size=1" -var="max_size=5"  # Valid
   terraform apply -var="min_size=5" -var="max_size=3"  # Invalid, triggers cross-variable validation error
   ```

### Complete File Contents

#### main.tf

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-XX"
  }
}
```

#### variables.tf

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}
```

#### dynamodb.tf

```hcl
# dynamodb.tf

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = var.dynamodb_billing_mode

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

#### vpc.tf

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}
```
