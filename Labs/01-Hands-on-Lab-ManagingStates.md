### Hands-on Lab: Setting Up and Managing State in Terraform

#### Intro to the Hands-on Lab
In this lab, you will learn how to set up and manage state in Terraform, an essential aspect for maintaining accurate and consistent infrastructure. This lab will guide you through configuring local and remote state backends, implementing state locking, importing existing resources, and recovering lost state. By the end of this lab, you will have a solid understanding of best practices for managing Terraform state, ensuring your infrastructure is both reliable and secure.

#### Explanation
Terraform state is a critical component that tracks the current state of your infrastructure. It serves as a source of truth for Terraform, enabling it to determine what changes need to be applied to match your configuration files with the real-world infrastructure.

1. **Local vs. Remote State:**
   - Local state is stored on your local disk in a file called `terraform.tfstate`. It's simple to set up but not ideal for collaborative environments.
   - Remote state stores the state file in a remote location, such as an AWS S3 bucket, allowing for better collaboration and state locking to prevent concurrent modifications.

2. **State Locking:**
   - State locking ensures that only one operation can modify the state at a time, preventing conflicts and potential corruption.
   - When using remote state with backends like AWS S3, DynamoDB can be used to enable state locking.

3. **Importing Existing Resources:**
   - Terraform can import existing resources that were not created by Terraform. This is useful for bringing manually created infrastructure under Terraform management.
   - Use the `terraform import` command to import resources.

4. **Recovering Lost State:**
   - Regular backups of the state file are crucial.
   - If local state is lost, it can be reconstructed using the `terraform import` command or by retrieving the state from remote storage.

**Example Code:**

_Configuring Remote State with AWS S3 and DynamoDB:_

```hcl
terraform {
  backend "s3" {
    bucket         = "your-terraform-state-bucket"
    key            = "path/to/your/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "your-dynamodb-table"
    encrypt        = true
  }
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "your-terraform-state-bucket"
  acl    = "private"
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "your-dynamodb-table"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}
```

#### Primary Assignment
Your primary assignment is to set up a remote state backend for Terraform using AWS S3 and DynamoDB for state locking. Follow these steps:

1. Use the provided S3 bucket for state storage.
  - Bucket name: `cprimelearning-tflabs-XX`
3. Create a DynamoDB table for state locking.
   - Table name: `terraform-state-lock-XX`
5. Configure your Terraform project to use the remote state backend.
6. Apply your configuration to verify that the state is stored remotely and that locking is enabled.

**Hints:**
- 
- Use the example code provided to guide your setup.
- Verify the state file is created in the S3 bucket and that the DynamoDB table records locks during `terraform apply` operations.

#### Optional Secondary Assignments
1. **Import Existing Resources:**
   - Import an existing AWS resource (e.g., an EC2 instance) into Terraform.
   - Write the corresponding Terraform configuration for the imported resource.

2. **State Integrity Check:**
   - **Implement checksums** to ensure the integrity of the state file stored in S3.
   - **Configure a mechanism** to verify the checksum before applying changes to ensure that the state file has not been tampered with.
   - **Create a script** that automates the verification process and integrates it into your Terraform workflow.

3.  **State Snapshot Management:**
   - **Create a mechanism** to take periodic snapshots of your Terraform state file.
   - **Store these snapshots** in a secure location separate from your primary state file.
   - **Develop a procedure** to restore the state from a snapshot in case of state file corruption or loss.

#### Lab Summary
In this lab, you have learned how to set up and manage Terraform state using both local and remote backends. You configured an AWS S3 bucket and a DynamoDB table to store state and enable state locking, ensuring the integrity and security of your infrastructure state. Additionally, you explored advanced topics such as importing existing resources and recovering lost state, equipping you with the knowledge to handle real-world Terraform state management challenges effectively.

### More Information

For additional resources and detailed documentation to help you complete both the Primary and Optional Secondary Assignments, refer to the following links:

1. **Terraform State Management:**
   - [Terraform State Documentation](https://www.terraform.io/docs/state/index.html)
   - [Remote State Storage](https://www.terraform.io/docs/state/remote.html)
   - [State Locking](https://www.terraform.io/docs/state/locking.html)
   - [State Import](https://www.terraform.io/docs/commands/import.html)
   - [State Recovery](https://www.terraform.io/docs/state/pull.html)

2. **AWS S3 and DynamoDB for Remote State:**
   - [AWS S3 Backend](https://www.terraform.io/docs/backends/types/s3.html)
   - [AWS DynamoDB for State Locking](https://www.terraform.io/docs/backends/types/s3.html#dynamodb-table-based-state-locking)
   - [AWS CLI for S3](https://docs.aws.amazon.com/cli/latest/reference/s3/index.html)
   - [AWS CLI for DynamoDB](https://docs.aws.amazon.com/cli/latest/reference/dynamodb/index.html)

3. **Security Considerations:**
   - [Managing Sensitive Data](https://www.terraform.io/docs/configuration/variables.html#sensitive-variables)
   - [Environment Variables](https://www.terraform.io/docs/cli/config/environment-variables.html)

These resources provide comprehensive information and examples that will assist you in successfully completing the lab assignments and deepening your understanding of Terraform state management.
