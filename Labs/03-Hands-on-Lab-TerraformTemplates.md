### Hands-on Lab: Terraform Templates

#### Intro to the Hands-on Lab
In this lab, you will learn how to work with templates in Terraform. You will set up templates, map them with various data sources, utilize external data sources, and build and configure template files. By the end of this lab, you will have a solid understanding of how to use templates to create dynamic and flexible configurations in Terraform.

### Section 1: Setting up Templates in Terraform

#### Explanation
Templates in Terraform allow you to create dynamic and reusable configurations. The `templatefile` function is used to read and render template files, which can include variables and expressions to generate customized content.

1. **Templatefile Function:**
   - Reads a file and renders it as a string.
   - Allows the use of variables and expressions within the template.

**Example Usage:**
_Template File (example.tftpl):_
```hcl
resource "aws_instance" "example" {
  count         = var.instance_count
  ami           = var.ami_id
  instance_type = var.instance_type
}
```

_Main Configuration:_
```hcl
variable "instance_count" {}
variable "ami_id" {}
variable "instance_type" {}

output "template_output" {
  value = templatefile("${path.module}/example.tftpl", {
    instance_count = var.instance_count
    ami_id         = var.ami_id
    instance_type  = var.instance_type
  })
}
```

#### Primary Assignment
Set up templates in Terraform to generate dynamic resource configurations. Complete the following tasks:

1. Create a template file that will be used to add a file added to an AWS instance.
2. Use the `templatefile` function to include that file from the template with variable values in an `aws_instance`.
3. Output the rendered template content and verify its correctness.

**Hints:**
- Create a template file (`example.tftpl`) with placeholder variables.
- Define the corresponding variables in your main configuration.
- Use the `templatefile` function to render the template and output the result.

#### Optional Secondary Assignments
1. **Complex Templates:**
   - Create a complex template that includes conditionals and loops.
   - Render the template and verify the generated content.

2. **Template Reuse:**
   - Create multiple templates for different resource types.
   - Use the `templatefile` function to render each template with appropriate variables.

3. **Template Functions:**
   - Explore and use different template functions (e.g., `join`, `split`).
   - Document their usage and impact on the generated content.

### Section 2: Mapping Templates with Various Data Sources

#### Explanation
Data sources in Terraform allow you to retrieve information from external systems. By mapping templates with data sources, you can dynamically generate content based on external data.

1. **Using Data Sources:**
   - Data sources are used to query external information and use it within Terraform configurations.
   - Common data sources include AWS, Azure, and GCP services.

**Example Usage:**
```hcl
data "aws_ami" "example" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = ["my-ami-*"]
  }
}

resource "aws_instance" "example" {
  ami           = data.aws_ami.example.id
  instance_type = var.instance_type
}
```

#### Primary Assignment
Map templates with various data sources in Terraform. Complete the following tasks:

1. Create a data source to retrieve the latest AWS AMI.
2. Use the retrieved data in a template to configure an AWS instance.
3. Render the template and apply the configuration.

**Hints:**
- Define a data source for the AWS AMI.
- Use the `templatefile` function to include the AMI ID in the template.
- Verify the rendered content and deploy the instance.

#### Optional Secondary Assignments
1. **Multiple Data Sources:**
   - Use multiple data sources to retrieve information from different services.
   - Map the retrieved data to a single template and generate the configuration.

2. **Dynamic Resource Names:**
   - Use data sources to dynamically generate resource names.
   - Render the template with the dynamic names and verify the result.

3. **External API Integration:**
   - Integrate an external API as a data source.
   - Use the data retrieved from the API to generate template content.

### Section 3: Utilizing External Data Sources

#### Explanation
External data sources allow Terraform to interact with custom or third-party data providers, expanding its capabilities beyond built-in data sources.

1. **Creating External Data Sources:**
   - External data sources can be created using external providers or custom scripts.
   - The `external` data source allows executing a program to fetch data.

**Example Usage:**
```hcl
data "external" "example" {
  program = ["python", "${path.module}/get_data.py"]

  query = {
    key = "value"
  }
}
```

_Script (get_data.py):_
```python
#!/usr/bin/env python
import json

def main():
    query = json.loads(input())
    # Fetch or compute the data based on the query
    data = {
        "result": "computed_value"
    }
    print(json.dumps(data))

if __name__ == "__main__":
    main()
```

#### Primary Assignment
Utilize external data sources in Terraform. Complete the following tasks:

1. Create a custom script to fetch external data.
2. Define an external data source in Terraform using the custom script.
3. Use the retrieved data in a template to generate resource configurations.

**Hints:**
- Write a script that outputs data in JSON format.
- Use the `external` data source to execute the script and retrieve the data.
- Include the data in a template using the `templatefile` function.

#### Optional Secondary Assignments
1. **Custom Provider:**
   - Implement a custom data provider in Terraform.
   - Use the custom provider to fetch data and include it in a template.

2. **Data Transformation:**
   - Transform the data retrieved from an external source before using it in a template.
   - Document the transformation process and its impact on the configuration.

3. **External API Data:**
   - Fetch data from an external API using a custom script.
   - Use the retrieved API data to generate dynamic content in a template.

### Section 4: Building and Configuring Template Files

#### Explanation
Building and configuring template files allows for the creation of complex and reusable Terraform configurations. Templates can include variables, conditionals, and loops to generate dynamic content.

1. **Template File Syntax:**
   - Templates can include variables using `${}` syntax.
   - Conditionals and loops can be used to generate content based on variable values.

**Example Usage:**
_Template File (example.tftpl):_
```hcl
%{ for instance in instances ~}
resource "aws_instance" "${instance.name}" {
  ami           = "${instance.ami}"
  instance_type = "${instance.type}"
}
%{ endfor ~}
```

_Main Configuration:_
```hcl
variable "instances" {
  type = list(object({
    name = string
    ami  = string
    type = string
  }))
}

output "template_output" {
  value = templatefile("${path.module}/example.tftpl", {
    instances = var.instances
  })
}
```

#### Primary Assignment
Build and configure template files in Terraform. Complete the following tasks:

1. Create a complex template file with variables, conditionals, and loops.
2. Define the necessary variables in the main configuration.
3. Render the template and verify the generated content.

**Hints:**
- Use the provided example to create a template file with loops and conditionals.
- Define a list of objects as the variable for instances.
- Render the template and output the result.

#### Optional Secondary Assignments
1. **Nested Templates:**
   - Create nested templates where one template includes another.
   - Render the nested templates and verify the generated content.

2. **Advanced Conditionals:**
   - Implement advanced conditionals in the template to generate different configurations based on variable values.
   - Document the conditional logic and its impact on the configuration.

3. **Template Reusability:**
   - Create reusable template files for different resource types.
   - Use the reusable templates in multiple configurations and verify their flexibility.

#### Lab Summary
In this lab, you set up templates in Terraform, mapped them with various data sources, utilized external data sources, and built and configured complex template files. These skills are essential for creating dynamic and flexible Terraform configurations that can adapt to different environments and requirements. By completing the primary and optional assignments, you have gained hands-on experience with key Terraform template concepts that will enhance your ability to manage infrastructure as code effectively.

### More Information

For additional resources and detailed documentation to help you complete both the Primary and Optional Secondary Assignments, refer to the following links:

1. **Terraform Templates:**
   - [Terraform Templatefile Function](https://www.terraform.io/docs/language/functions/templatefile.html)
   - [Terraform Configuration Language](https://www.terraform.io/docs/language/syntax/configuration.html)

2. **Data Sources:**
   - [Terraform Data Sources](https://www.terraform.io/docs/language/data-sources/index.html)
   - [AWS Data Sources](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources)
   - [External Data Source](https://www.terraform.io/docs/language/data-sources/external.html)

3. **Using External Data Sources:**
   - [Terraform External Provider](https://www.terraform.io/docs/providers/external/index.html)
   - [Example External Data Source](https://www.terraform.io/docs/providers/external/d/data_source.html)

4. **Building and Configuring Template Files:**
   - [Terraform Templatefile Function](https://www.terraform.io/docs/language/functions/templatefile.html)
   - [Using Variables in Templates](https://www.terraform.io/docs/language/values/variables.html)

These resources provide comprehensive information and examples that will assist you in successfully completing the lab assignments and deepening your understanding of Terraform templates, data sources, external data sources, and building and configuring template files.
