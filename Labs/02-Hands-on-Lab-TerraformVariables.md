### Hands-on Lab: Variables and Locals

#### Intro to the Hands-on Lab
In this lab, you will delve into Terraform expressions, explore various variable input options, and test variables with validation. By the end of this lab, you will have a deeper understanding of how to utilize Terraform expressions, manage different variable input methods, and implement variable validation to ensure your configurations are robust and flexible.

### Section 1: Exploring Terraform Expressions with Terraform Console

#### Explanation
Terraform expressions are a powerful way to define and manipulate values within your configurations. The Terraform console is an interactive tool that allows you to evaluate expressions, test configurations, and debug issues in real-time.

1. **Terraform Console:**
   - The Terraform console provides an interactive REPL (Read-Eval-Print Loop) for evaluating expressions.
   - It helps in testing and debugging configurations without applying changes.
   - Common commands include evaluating variable values, testing conditionals, and debugging functions.

**Example Usage:**
```sh
$ terraform console
> var.my_variable
> aws_instance.my_instance.public_ip
> length(list("a", "b", "c"))
```

#### Primary Assignment
Use the Terraform console to explore and evaluate various Terraform expressions. Perform the following tasks:

1. Create and migrate values to variables. (`var.vcp_cider_block` & `var.dynamodb_billing_mode`)
2. Launch the Terraform console from your lab machine.
3. Evaluate the values of existing variables and resources.
4. Test conditional expressions and functions.
   - Test a simple conditional expression
   - Test the length function
   - Test the coalesce function
   - Test a custom function to count the number of subnets

**Hints:**
- Use the `terraform console` command to start the console.
- Try evaluating different types of expressions, such as lists, maps, and conditionals.
- Use the `var.<variable_name>` syntax to evaluate variables.

#### Optional Secondary Assignments
1. **Complex Expressions:**
   - Create and evaluate complex expressions involving multiple functions and conditionals.
   - Document the evaluated results and explain each step.

2. **Custom Functions:**
   - Write and test custom functions using the Terraform console.
   - Explore the capabilities of built-in functions like `join`, `split`, and `lookup`.

3. **Debugging:**
   - Use the Terraform console to debug a configuration with syntax errors.
   - Identify and fix the errors based on the console output.

### Section 2: Utilize Various Variable Input Options

#### Explanation
Terraform allows for flexible variable input methods, including CLI variables, environment variables, and variable files. Understanding how to use these input options effectively can simplify and secure your configurations.

1. **Variable Input Methods:**
   - **CLI Variables:** Passed directly via the command line using `-var` flags.
   - **Environment Variables:** Set using the `TF_VAR_` prefix.
   - **Variable Files:** Defined in `.tfvars` files.

**Example Usage:**
```sh
$ terraform apply -var 'instance_type=t2.micro'
$ export TF_VAR_instance_type=t2.micro
$ terraform apply -var-file="variables.tfvars"
```

#### Primary Assignment
Utilize various variable input options to configure a Terraform project. Complete the following tasks:

1. Define variables in the configuration file.
2. Override variables using CLI arguments, environment variables, and variable files.
3. Apply the configuration and verify the variable values.

**Hints:**
- Create a `variables.tf` file to define the variables.
- Use different methods to override the variable values and observe the changes.

#### Optional Secondary Assignments
1. **Variable Precedence:**
   - Experiment with variable precedence by setting the same variable using different methods.
   - Document which method takes precedence in various scenarios.

2. **Sensitive Variables:**
   - Mark variables as sensitive and observe how Terraform handles them.
   - Test different ways to securely pass sensitive variables.

3. **Default Values:**
   - Define default values for variables and override them.
   - Test how default values behave when no override is provided.

### Section 3: Test Variables with Validation

#### Explanation
Variable validation in Terraform ensures that input values meet specific criteria, preventing misconfigurations and errors. Validation rules can be defined using the `validation` block within variable definitions.

1. **Variable Validation:**
   - Define validation rules to check variable values.
   - Use conditionals and custom error messages to enforce validation.

**Example Usage:**
```hcl
variable "instance_count" {
  type    = number
  default = 1

  validation {
    condition     = var.instance_count > 0 && var.instance_count <= 10
    error_message = "Instance count must be between 1 and 10."
  }
}
```

#### Primary Assignment
Implement variable validation in a Terraform project. Complete the following tasks:

1. Define variables with validation rules.
2. Test the validation rules by providing different input values.
3. Apply the configuration and observe the validation behavior.

**Hints:**
- Use the `validation` block within your variable definitions.
- Test various input values to trigger validation errors.

#### Optional Secondary Assignments
1. **Complex Validation:**
   - Implement complex validation rules involving multiple conditions.
   - Document the validation logic and test cases.

2. **Custom Error Messages:**
   - Define custom error messages for validation failures.
   - Test the error messages to ensure they provide clear guidance.

3. **Cross-variable Validation:**
   - Implement validation rules that depend on the values of other variables.
   - Test scenarios where cross-variable dependencies are involved.

#### Lab Summary
In this lab, you explored Terraform expressions using the Terraform console, utilized various variable input options, and implemented variable validation to ensure robust configurations. These skills are essential for managing complex Terraform projects effectively and securely. By completing the primary and optional assignments, you have gained hands-on experience with key Terraform concepts that will enhance your ability to manage infrastructure as code.

### More Information

For additional resources and detailed documentation to help you complete both the Primary and Optional Secondary Assignments, refer to the following links:

1. **Terraform Expressions and Console:**
   - [Terraform Expressions Documentation](https://www.terraform.io/docs/language/expressions/index.html)
   - [Terraform Console](https://www.terraform.io/docs/cli/commands/console.html)
   - [Terraform Functions](https://www.terraform.io/docs/language/functions/index.html)
   - [Terraform Conditional Expressions](https://www.terraform.io/docs/language/expressions/conditionals.html)

2. **Variable Input Options:**
   - [Input Variables](https://www.terraform.io/docs/language/values/variables.html)
   - [Environment Variables](https://www.terraform.io/docs/cli/config/environment-variables.html)
   - [Variable Files](https://www.terraform.io/docs/language/values/variables.html#variable-definitions-tfvars-files)
   - [CLI Arguments](https://www.terraform.io/docs/cli/commands/apply.html#var-and-var-file-options)

3. **Variable Validation:**
   - [Variable Validation](https://www.terraform.io/docs/language/values/variables.html#validation)
   - [Custom Validation Rules](https://www.terraform.io/docs/language/expressions/custom-conditions.html)

These resources provide comprehensive information and examples that will assist you in successfully completing the lab assignments and deepening your understanding of Terraform expressions, variable input options, and variable validation.
