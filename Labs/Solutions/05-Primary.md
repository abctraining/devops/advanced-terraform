### Hands-on Lab Solution: Performance Optimization and Troubleshooting in Terraform

The primary assignment for this lab is divided into five sections. Each section involves specific tasks related to optimizing configurations, debugging and fixing code, scaling and refactoring setups, using advanced CLI commands, and upgrading Terraform versions.

---

### Section 1: Optimizing Terraform Configurations for Performance

#### Primary Assignment

Optimize a Terraform configuration for performance. Complete the following tasks:

1. Identify resource dependencies and minimize unnecessary ones.
2. Use targeted updates to apply changes to specific resources.
3. Optimize state management by moving the DynamoDB table resource to a separate stand-alone Terraform project.

### Step-by-Step Solution for Section 1

#### Step 1: Identify and Minimize Unnecessary Resource Dependencies

Review the existing dependencies in the configuration files and remove or minimize any unnecessary dependencies.

Set the value for `var.ingress_rules` in `terraform.tfvars` to only allow HTTPS traffic:

```hcl
# terraform.tfvars

ingress_rules = [
  {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
]
```

Update the `security_group.tf` file if needed:

```hcl
# security_group.tf

resource "aws_security_group" "example" {
  name        = "example_sg"
  vpc_id      = aws_vpc.example.id
  description = "Example security group with dynamic ingress rules"

  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }
}
```

#### Step 2: Use Targeted Updates

Use targeted updates to apply changes to specific resources. For example, if you need to update only the security group, you can target it using:

```sh
terraform apply -target=aws_security_group.example
```

#### Step 3: Optimize State Management

Move the DynamoDB table resource to a separate stand-alone Terraform project.

1. Remove the DynamoDB locking from the Terraform block in `main.tf`:

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "cprimelearning-tflabs-XX"
    key    = "terraform/state"
    region = "us-west-2"
  }
}
```

2. Create a new directory `dynamodb_project` and move the DynamoDB resource definition there:

```sh
mkdir ../dynamodb_project
mv dynamodb.tf ../dynamodb_project/
cp main.tf ../dynamodb_project/
```

3. Still in the Current project reconfigure and apply the changes to remove the DynamoDB table from the state:

```sh
terraform init -reconfigure
terraform apply
```

4. Modify the S3 State key,  Define the `var.dynamodb_billing_mode`, Initialize and apply the configuration in the new `dynamodb_project` directory:

```sh
cd ../dynamodb_project
```

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/locking-state"
    region         = "us-west-2"
  }
}
```

```hcl
# variables.tf

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}
```

4. Initialize and apply the configuration in the new `dynamodb_project` directory:

```sh
terraform init
terraform apply
```

5. After the DynamoDB resource is managed by the separate project, you can update the `main.tf` in the original project to re-enable DynamoDB locking:

```sh
cd ../<original_project_folder>
```

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-XX"
  }
}
```

6. In the Current project reconfigure and apply the changes to enable the locking in DynamoDB again:

```sh
terraform init -reconfigure
terraform apply
```

7. Remove the 'dynamodb_billing_mode' variable from the project.

The variable 'dynamodb_billing_mode' is no longer needed in this project .  Remove it from the `variables.tf` file.
### Section 2: Debugging and Fixing Terraform Code

#### Primary Assignment

Debug and fix a Terraform configuration. Complete the following tasks:

1. Identify and resolve errors in the configuration using `terraform plan`.
2. Enable and analyze detailed logs to troubleshoot issues.
3. Use the Terraform console to interactively debug the configuration.

### Step-by-Step Solution for Section 2

#### Step 1: Identify and Resolve Errors

Run `terraform plan` to identify any configuration errors.

```sh
terraform plan
```

Resolve any errors that appear in the output. For example, if you encounter an undefined variable, define it in the `variables.tf` file.

#### Step 2: Enable Detailed Logs

Enable detailed logs by setting the `TF_LOG` environment variable.

```sh
export TF_LOG=DEBUG
terraform plan
```

Analyze the logs to identify issues.

#### Step 3: Use the Terraform Console

Use the Terraform console to debug the configuration interactively.

```sh
terraform console
> aws_vpc.example.id
```

### Section 3: Using Advanced Terraform CLI Commands

#### Primary Assignment

Use advanced Terraform CLI commands to manage a configuration. Complete the following tasks:

1. Use `terraform taint` and `untaint` to mark resources for recreation.
2. Inspect and manipulate the state file using `terraform state` commands.
3. Visualize resource dependencies using `terraform graph`.

### Step-by-Step Solution for Section 3

#### Step 1: Use `terraform taint` and `untaint`

Mark a resource for recreation.

```sh
terraform taint aws_instance.example
terraform apply
```

Unmark the resource.

```sh
terraform untaint aws_instance.example
terraform apply
```

#### Step 2: Inspect and Manipulate the State File

List resources in the state file.

```sh
terraform state list
```

Show details about a specific resource.

```sh
terraform state show aws_instance.example
```

Move a resource in the state file.

```sh
terraform state mv aws_instance.example aws_instance.new_example
```

#### Step 3: Visualize Resource Dependencies

Generate a graph of the resource dependencies.

```sh
terraform graph
```

If you have a graphical desktop and have Graphviz installed you can convent that `terraform graph` data to an image to visualize it.

```sh
terraform graph | dot -Tpng > graph.png
```


### Section 4: Upgrading Terraform Versions and Managing Changes

#### Primary Assignment

Upgrade Terraform versions and manage changes. Complete the following tasks:

1. Define version constraints for Terraform and providers.
2. Upgrade to a newer version of Terraform and apply necessary migration steps.
3. Test the configuration to ensure compatibility with the new version.

### Step-by-Step Solution for Section 4

#### Step 1: Define Version Constraints

Add version constraints to the `main.tf` file.

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-00"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-00"
  }
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }
  }
}
```

#### Step 2: Upgrade to a Newer Version

Upgrade Terraform and apply necessary migration steps.

```sh
terraform init -upgrade
terraform apply
```

#### Step 3: Test the Configuration

Run `terraform plan` and `terraform apply` to ensure compatibility.

```sh
terraform plan
terraform apply
```

### Complete File Contents

#### main.tf

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-00"
    key            = "terraform/state"
    region         = "us-west-2"
    dynamodb_table = "terraform-state-lock-00"
  }
  required_version = ">= 1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }
  }
}
```

#### variables.tf

```hcl
# variables.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
  validation {
    condition     = can(regex("^\\d+

\\.\\d+\\.\\d+\\.\\d+/\\d+$", var.vpc_cidr_block))
    error_message = "The vpc_cidr_block value must be a valid CIDR block."
  }
}

variable "user_name" {
  description = "The name to include in the template"
  type        = string
  default     = "Terraform User"
}

variable "ingress_rules" {
  description = "List of ingress rules for the security group"
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
  default = [
    {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  ]
}

variable "environment" {
  description = "The environment for the resources (e.g., dev, prod)"
  type        = string
  default     = "dev"
}

variable "instance_count" {
  description = "The number of instances to create"
  type        = number
  default     = 2
}

variable "instance_types" {
  description = "Map of instance types with instance names"
  type = map(string)
  default = {
    instance1 = "t2.micro"
    instance2 = "t2.small"
  }
}

variable "vpc_count" {
  description = "The number of VPCs to create"
  type        = number
  default     = 1
}

variable "server_name" {
  description = "The name of the server"
  type        = string
  default     = "default_server"
}

variable "server_ip" {
  description = "The IP address of the server"
  type        = string
  default     = "192.168.1.1"
}

variable "server_count" {
  description = "The number of servers"
  type        = number
  default     = 1
}
```

#### vpc.tf

```hcl
# vpc.tf

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
  count      = var.vpc_count
}

resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-2a"
  count             = var.vpc_count
}
```

#### security_group.tf

```hcl
# security_group.tf

resource "aws_security_group" "example" {
  name        = "example_sg"
  vpc_id      = aws_vpc.example.id
  description = "Example security group with dynamic ingress rules"

  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }
}
```

#### template.tf

```hcl
# template.tf

data "template_file" "user_data" {
  template = file("${path.module}/user_data.tpl")
  vars = {
    user_name = data.external.custom_data.result.user_name
  }
}

data "template_file" "complex" {
  template = file("${path.module}/complex_template.tpl")
  vars = {
    server_name  = var.server_name,
    server_ip    = var.server_ip,
    server_count = var.server_count
  }
}

resource "aws_instance" "example" {
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = var.environment == "prod" ? "t2.small" : "t2.micro"
  subnet_id     = aws_subnet.example.id
  user_data     = data.template_file.user_data.rendered
}
```

#### instances_count.tf

```hcl
# instances_count.tf

resource "aws_instance" "count_example" {
  count         = var.instance_count
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.example.id
}
```

#### instances_for_each.tf

```hcl
# instances_for_each.tf

resource "aws_instance" "for_each_example" {
  for_each      = var.instance_types
  ami           = data.aws_ami.latest_amazon_linux.id
  instance_type = each.value
  subnet_id     = aws_subnet.example.id
}
```

#### modules.tf

```hcl
# modules.tf

module "example" {
  source        = "./modules/example"
  instance_type = "t2.micro"
  ami_id        = data.aws_ami.latest_amazon_linux.id
  subnet_id     = aws_subnet.example.id
}
```

#### outputs.tf

```hcl
# outputs.tf

output "rendered_template" {
  value = data.template_file.user_data.rendered
}

output "rendered_complex_template" {
  value = data.template_file.complex.rendered
}
```

#### datasources.tf

```hcl
# datasources.tf

data "aws_ami" "latest_amazon_linux" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}
```

#### external.tf

```hcl
# external.tf

data "external" "custom_data" {
  program = ["${path.module}/fetch_external_data.sh"]
}
```

#### user_data.tpl

```hcl
# user_data.tpl

#!/bin/bash
echo "Hello, ${user_name}! Welcome to your new instance." > /home/ec2-user/welcome.txt
```

#### complex_template.tpl

```hcl
# complex_template.tpl

%{ if server_name != "" }
server {
  name = "${server_name}"
  ip   = "${server_ip}"
}
%{ endif }

%{ if server_count > 1 }
multiple_servers = true
%{ else }
multiple_servers = false
%{ endif }
```

#### fetch_external_data.sh

```sh
#!/bin/bash
echo '{"user_name": "External User"}'
```

#### modules/example/main.tf

```hcl
# modules/example/main.tf

variable "instance_type" {
  description = "The instance type for the EC2 instance"
  type        = string
  default     = "t2.micro"
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}

variable "subnet_id" {
  description = "The subnet ID for the instance"
  type        = string
}

resource "aws_instance" "example" {
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
}
```

#### modules/example/versions.tf

```hcl
# modules/example/versions.tf

terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.0"
    }
  }
}
```

#### modules/network/main.tf

```hcl
# modules/network/main.tf

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

resource "aws_vpc" "example" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "example" {
  vpc_id            = aws_vpc.example.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-2a"
}
```

#### modules/network/outputs.tf

```hcl
# modules/network/outputs.tf

output "vpc_id" {
  value = aws_vpc.example.id
}

output "subnet_id" {
  value = aws_subnet.example.id
}
```

#### modules/compute/main.tf

```hcl
# modules/compute/main.tf

variable "instance_type" {
  description = "The instance type for the EC2 instance"
  type        = string
  default     = "t2.micro"
}

variable "subnet_id" {
  description = "The subnet ID for the instance"
 

 type        = string
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}

resource "aws_instance" "example" {
  ami           = var.ami_id
  instance_type = var.instance_type
  subnet_id     = var.subnet_id
}
```

#### modules/compute/variables.tf

```hcl
# modules/compute/variables.tf

variable "subnet_id" {
  description = "The subnet ID for the instance"
  type        = string
}

variable "ami_id" {
  description = "The AMI ID for the instance"
  type        = string
}
```

#### modules/composite/main.tf

```hcl
# modules/composite/main.tf

module "network" {
  source = "../network"
}

module "compute" {
  source        = "../compute"
  instance_type = "t2.micro"
  subnet_id     = module.network.subnet_id
  ami_id        = data.aws_ami.latest_amazon_linux.id
}
```

#### ../dynamodb_project/main.tf (stand-alone project)

```hcl
# main.tf

provider "aws" {
  region = "us-west-2"
}

terraform {
  backend "s3" {
    bucket         = "cprimelearning-tflabs-XX"
    key            = "terraform/locking-state"
    region         = "us-west-2"
  }
}
```

#### ../dynamodb_project/dynomodb.tf (stand-alone project)
```hcl
# dynamodb.tf

provider "aws" {
  region = "us-west-2"
}

resource "aws_dynamodb_table" "terraform_state_lock" {
  name         = "terraform-state-lock-XX"
  billing_mode = var.dynamodb_billing_mode

  attribute {
    name = "LockID"
    type = "S"
  }

  hash_key = "LockID"
}
```

#### ../dynamodb_project/variables.tf (stand-alone project)
```hcl
# variables.tf

variable "dynamodb_billing_mode" {
  description = "The billing mode for the DynamoDB table"
  type        = string
  default     = "PAY_PER_REQUEST"
  validation {
    condition     = contains(["PAY_PER_REQUEST", "PROVISIONED"], var.dynamodb_billing_mode)
    error_message = "The dynamodb_billing_mode must be either 'PAY_PER_REQUEST' or 'PROVISIONED'."
  }
}
```

### Summary

You have successfully completed the primary assignments for the "Hands-on Lab: Performance Optimization and Troubleshooting in Terraform," including optimizing configurations, debugging and fixing code, refactoring setups, using advanced CLI commands, and upgrading Terraform versions. This comprehensive approach ensures that you are proficient in optimizing and managing Terraform projects efficiently.
