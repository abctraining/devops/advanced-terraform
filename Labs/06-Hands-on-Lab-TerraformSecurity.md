### Hands-on Lab: Security Configurations in Terraform

#### Intro to the Hands-on Lab
In this lab, you will explore security considerations in Terraform infrastructure, set up privileges within your architecture, configure secure networks, and manage advanced firewall and security group rules. By the end of this lab, you will have a comprehensive understanding of how to secure your Terraform-managed infrastructure effectively.

### Section 1: Security Considerations with Terraform Infrastructure

#### Explanation
Security is a critical aspect of managing infrastructure with Terraform. Properly handling sensitive data, implementing least privilege principles, and securing state files are essential practices.

1. **Managing Sensitive Data:**
   - Use environment variables and secure backends to manage sensitive data.
   - Terraform supports sensitive inputs to mark variables as sensitive.

2. **Implementing Least Privilege:**
   - Define IAM policies and roles with the least privilege necessary for resources.
   - Regularly review and audit permissions.

3. **Securing State Files:**
   - Store state files in a secure remote backend like AWS S3 with encryption enabled.
   - Enable state locking using DynamoDB to prevent concurrent modifications.

**Example Usage:**
```hcl
variable "db_password" {
  type      = string
  sensitive = true
}

resource "aws_secretsmanager_secret" "example" {
  name        = "example"
  description = "Example secret"
}

resource "aws_secretsmanager_secret_version" "example" {
  secret_id     = aws_secretsmanager_secret.example.id
  secret_string = var.db_password
}
```

#### Primary Assignment
Implement security considerations in a Terraform configuration. Complete the following tasks:

1. Store sensitive data securely using environment variables and secure backends.
2. Define IAM policies and roles following the principle of least privilege.
3. Secure state files by configuring a remote backend with encryption and state locking.

**Hints:**
- Use the `sensitive` argument in variable definitions to mark sensitive data.
- Configure AWS IAM policies with the least privilege required.
- Store state files in an encrypted S3 bucket and enable state locking with DynamoDB.

#### Optional Secondary Assignments
1. **Sensitive Data Management:**
   - Store sensitive data in AWS Secrets Manager or Parameter Store.
   - Reference the stored secrets in your Terraform configuration.

2. **Permission Audits:**
   - Implement regular IAM policy audits using AWS IAM Access Analyzer.
   - Document any changes made to ensure least privilege.

3. **State File Encryption:**
   - Ensure that the S3 bucket used for state files has server-side encryption enabled.
   - Verify the encryption status of the state files in the S3 bucket.

### Section 2: Setting up Privileges within Your Architecture

#### Explanation
Setting up privileges within your architecture involves defining and managing IAM roles and policies to ensure that resources have only the permissions they need. This reduces the risk of unauthorized access and potential security breaches.

1. **Defining IAM Roles and Policies:**
   - Create IAM roles and attach policies with the least privilege required.
   - Use IAM policy documents to specify allowed actions and resources.

2. **Managing Role Assumptions:**
   - Define trust relationships to allow roles to be assumed by specific entities.
   - Use IAM policies to grant assume role permissions.

**Example Usage:**
```hcl
resource "aws_iam_role" "example" {
  name = "example-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_policy" "example" {
  name        = "example-policy"
  description = "Example policy"
  policy      = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:ListBucket",
          "s3:GetObject"
        ]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_role_policy_attachment" "example" {
  role       = aws_iam_role.example.name
  policy_arn = aws_iam_policy.example.arn
}
```

#### Primary Assignment
Set up privileges within your architecture using IAM roles and policies. Complete the following tasks:

1. Define an IAM role with a trust relationship.
2. Create and attach an IAM policy with the least privilege required.
3. Verify that the role and policy are correctly configured.

**Hints:**
- Use the `assume_role_policy` argument to define the trust relationship for the IAM role.
- Create an IAM policy document that specifies allowed actions and resources.
- Attach the policy to the role using an IAM role policy attachment.

#### Optional Secondary Assignments
1. **Cross-Account Role Assumption:**
   - Create a role that can be assumed by a different AWS account.
   - Test the role assumption from the other account.

2. **Policy Conditions:**
   - Implement IAM policy conditions to further restrict permissions.
   - Test the policy conditions by performing various actions.

3. **IAM Policy Simulator:**
   - Use the AWS IAM Policy Simulator to test and validate your IAM policies.
   - Document any necessary changes based on the simulation results.

### Section 3: Configuring Secure Networks

#### Explanation
Configuring secure networks involves setting up VPCs, subnets, and security groups to control access and ensure that your infrastructure is protected from unauthorized access.

1. **VPC and Subnet Configuration:**
   - Create VPCs and subnets to segment your network.
   - Use private subnets for resources that do not require internet access.

2. **Security Groups:**
   - Define security group rules to control inbound and outbound traffic.
   - Implement least privilege principles for security group rules.

**Example Usage:**
```hcl
resource "aws_vpc" "example" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "example" {
  vpc_id     = aws_vpc.example.id
  cidr_block = "10.0.1.0/24"
}

resource "aws_security_group" "example" {
  vpc_id = aws_vpc.example.id
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```

#### Primary Assignment
Configure a secure network in AWS using Terraform. Complete the following tasks:

1. Create a VPC and a subnet.
2. Define security group rules to control inbound and outbound traffic.
3. Apply the configuration and verify the network setup.

**Hints:**
- Use the `aws_vpc` and `aws_subnet` resources to create a VPC and subnet.
- Define ingress and egress rules for the security group.
- Apply the configuration and check the VPC and subnet properties.

#### Optional Secondary Assignments
1. **Private Subnets:**
   - Create private subnets and route traffic through a NAT gateway.
   - Test the connectivity of resources in private subnets.

2. **Network ACLs:**
   - Implement network ACLs to provide an additional layer of security.
   - Define rules to control traffic flow at the subnet level.

3. **VPC Peering:**
   - Set up VPC peering connections between different VPCs.
   - Verify the connectivity and security of the peered networks.

### Section 4: Advanced Firewall and Security Group Management

#### Explanation
Advanced firewall and security group management involves creating complex rules to control access to your resources. This includes defining fine-grained rules and using AWS WAF for web application security.

1. **Security Group Rules:**
   - Define detailed ingress and egress rules for security groups.
   - Use security group rules to enforce least privilege access.

2. **AWS WAF:**
   - Implement AWS WAF to protect web applications from common threats.
   - Define rules to block, allow, or monitor web requests.

**Example Usage:**
```hcl
resource "aws_waf_web_acl" "example" {
  name        = "example-waf"
  metric_name = "example-waf"

  default_action {
    type = "ALLOW"
  }

  rule {
    name     = "example-rule"
    priority = 1
    action {
      type = "BLOCK"
    }
    override_action {
      type = "NONE"
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "example-rule"
      sampled_requests_enabled   = true
    }
    statement {
      byte_match_statement {
        search_string = "example"
        field_to_match {
          uri_path {}
        }
        text_transformation {
          priority = 0
          type     = "NONE"
        }
        positional_constraint = "CONTAINS"
      }
    }
  }
}
```

#### Primary Assignment
Implement advanced firewall and security group management. Complete the following tasks:

1. Define detailed ingress and egress rules for a security group.
2. Set up AWS WAF to protect a web application.
3. Apply the configuration and verify the security rules.

**Hints:**
- Use the `aws_security_group` resource to define complex security group rules.
- Implement AWS WAF rules to protect against common web threats.
- Apply the configuration and test the security rules.

#### Optional Secondary Assignments
1. **Complex Security Group Rules:**
   - Define security group rules

 for different protocols and ports.
   - Test the rules by simulating various traffic scenarios.

2. **WAF Custom Rules:**
   - Create custom AWS WAF rules to block specific patterns.
   - Test the effectiveness of the custom rules on a web application.

3. **Security Automation:**
   - Automate the management of security groups and WAF rules using Terraform.
   - Implement automated testing to validate the security configurations.

#### Lab Summary
In this lab, you explored security considerations in Terraform infrastructure, set up privileges within your architecture, configured secure networks, and managed advanced firewall and security group rules. These skills are essential for securing your Terraform-managed infrastructure effectively. By completing the primary and optional assignments, you have gained hands-on experience with key Terraform security concepts that will enhance your ability to manage and protect your infrastructure as code.

### More Information

For additional resources and detailed documentation to help you complete both the Primary and Optional Secondary Assignments, refer to the following links:

1. **Security Considerations with Terraform Infrastructure:**
   - [Managing Sensitive Data in Terraform](https://www.terraform.io/docs/language/values/variables.html#sensitive-variables)
   - [Terraform Environment Variables](https://www.terraform.io/docs/cli/config/environment-variables.html)
   - [Terraform State](https://www.terraform.io/docs/state/index.html)
   - [Terraform AWS Provider: Secrets Manager](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/secretsmanager_secret)
   - [Terraform AWS Provider: IAM](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy)

2. **Setting Up Privileges within Your Architecture:**
   - [Terraform AWS Provider: IAM Role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role)
   - [Terraform AWS Provider: IAM Policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy)
   - [AWS IAM Best Practices](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html)

3. **Configuring Secure Networks:**
   - [Terraform AWS Provider: VPC](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc)
   - [Terraform AWS Provider: Subnet](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet)
   - [Terraform AWS Provider: Security Group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group)
   - [AWS VPC Best Practices](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Scenarios.html)

4. **Advanced Firewall and Security Group Management:**
   - [Terraform AWS Provider: Security Group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group)
   - [Terraform AWS Provider: AWS WAF](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/waf_web_acl)
   - [AWS WAF Documentation](https://docs.aws.amazon.com/waf/latest/developerguide/what-is-aws-waf.html)
   - [Advanced VPC Security](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-security.html)

These resources provide comprehensive information and examples that will assist you in successfully completing the lab assignments and deepening your understanding of security considerations, setting up privileges, configuring secure networks, and advanced firewall and security group management in Terraform.
