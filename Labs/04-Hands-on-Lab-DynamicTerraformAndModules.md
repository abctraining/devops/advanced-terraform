### Hands-on Lab: Dynamic Configurations and Advanced Module Techniques

#### Intro to the Hands-on Lab
In this lab, you will explore advanced Terraform techniques, including implementing dynamic blocks in resource configurations, utilizing conditional expressions, managing multiple instances of resources, versioning and releasing modules, and creating and managing composite modules. These techniques will help you create more flexible, reusable, and maintainable Terraform configurations.

### Section 1: Implementing Dynamic Blocks in Resource Configurations

#### Explanation
Dynamic blocks in Terraform allow you to create repeatable nested blocks within a resource, enabling more flexible and maintainable configurations. This is particularly useful when the number of nested blocks is not known ahead of time or can vary.

1. **Dynamic Blocks:**
   - The `dynamic` block allows you to dynamically construct nested blocks.
   - It requires a `for_each` argument and a `content` block to define the structure.

**Example Usage:**
```hcl
resource "aws_security_group" "example" {
  name = "example"

  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }
}

variable "ingress_rules" {
  type = list(object({
    from_port   = number
    to_port     = number
    protocol    = string
    cidr_blocks = list(string)
  }))
}
```

#### Primary Assignment
Implement dynamic blocks in a resource configuration. Complete the following tasks:

1. Define a variable to hold multiple ingress rules for a security group.
2. Use a dynamic block to create ingress rules based on the variable.
3. Apply the configuration and verify the dynamic block functionality.

**Hints:**
- Use the `dynamic` block within a resource definition.
- Define the `for_each` argument with a list of objects representing ingress rules.

#### Optional Secondary Assignments
1. **Advanced Dynamic Blocks:**
   - Implement dynamic blocks with conditionals inside the `content` block.
   - Test the configuration with different sets of input values.

2. **Nested Dynamic Blocks:**
   - Create nested dynamic blocks within a resource.
   - Verify the configuration by applying it and checking the resource properties.

3. **Dynamic Blocks with Modules:**
   - Use dynamic blocks within a module.
   - Pass variable data to the module and verify the dynamic block behavior.

### Section 2: Utilizing Conditional Expressions to Create Flexible Configurations

#### Explanation
Conditional expressions in Terraform allow you to create flexible and adaptive configurations based on variable values. They enable you to define resource properties conditionally, improving the adaptability of your configurations.

1. **Conditional Expressions:**
   - Use the ternary operator (`condition ? true_val : false_val`) for conditional expressions.
   - Apply conditionals to set resource properties based on variable values.

**Example Usage:**
```hcl
variable "environment" {
  type    = string
  default = "dev"
}

resource "aws_instance" "example" {
  ami           = "ami-12345678"
  instance_type = var.environment == "prod" ? "t2.large" : "t2.micro"
}
```

#### Primary Assignment
Utilize conditional expressions to create flexible configurations. Complete the following tasks:

1. Define a variable to specify the environment (e.g., `dev`, `prod`).
2. Use conditional expressions to set resource properties based on the environment.
3. Apply the configuration and verify the conditional behavior.

**Hints:**
- Use the ternary operator to set resource properties conditionally.
- Test the configuration with different environment values.

#### Optional Secondary Assignments
1. **Multiple Conditionals:**
   - Implement multiple conditional expressions within a single resource.
   - Test the configuration with various combinations of input values.

2. **Nested Conditionals:**
   - Create nested conditional expressions to handle complex scenarios.
   - Apply the configuration and verify the nested conditions.

3. **Conditional Modules:**
   - Use conditional expressions to conditionally include or exclude modules.
   - Test the behavior by applying the configuration with different input values.

### Section 3: Managing Multiple Instances of a Module or Resource with `count` and `for_each`

#### Explanation
Terraform's `count` and `for_each` arguments enable you to manage multiple instances of a resource or module efficiently. These arguments allow you to dynamically create resources based on variable input.

1. **Count and For_each:**
   - The `count` argument creates multiple instances based on a numeric value.
   - The `for_each` argument creates multiple instances based on a map or set of strings.

**Example Usage:**
```hcl
variable "instance_count" {
  type    = number
  default = 2
}

resource "aws_instance" "example" {
  count         = var.instance_count
  ami           = "ami-12345678"
  instance_type = "t2.micro"
}

variable "instances" {
  type = map(string)
}

resource "aws_instance" "example" {
  for_each = var.instances
  ami           = "ami-12345678"
  instance_type = each.value
}
```

#### Primary Assignment
Manage multiple instances of a resource using `count` and `for_each`. Complete the following tasks:

1. Define variables to specify the number of instances and their types.
2. Use `count` to create multiple instances based on the variable.
3. Use `for_each` to create instances based on a map of instance types.
4. Apply the configuration and verify the instances.

**Hints:**
- Use the `count` argument for numeric-based instance creation.
- Use the `for_each` argument for map-based instance creation.

#### Optional Secondary Assignments
1. **Advanced Count and For_each:**
   - Combine `count` and `for_each` in a single configuration.
   - Test the configuration with different sets of input values.

2. **Conditional Count and For_each:**
   - Use conditional expressions with `count` and `for_each`.
   - Verify the behavior by applying the configuration with various conditions.

3. **Dynamic Resources:**
   - Create dynamic resources using `count` and `for_each`.
   - Test the configuration with different dynamic resource sets.

### Section 4: Versioning and Releasing Modules

#### Explanation
Versioning and releasing modules in Terraform ensures stability and consistency in your infrastructure configurations. By versioning modules, you can manage changes and dependencies effectively.

1. **Module Versioning:**
   - Use version constraints to specify module versions.
   - Semantic versioning helps manage module updates and dependencies.

**Example Usage:**
```hcl
module "example" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.0.0"
  name    = "example"
  cidr    = "10.0.0.0/16"
}
```

#### Primary Assignment
Implement versioning and release a module in Terraform. Complete the following tasks:

1. Define a module with version constraints.
2. Create a versioned module and use it in a configuration.
3. Apply the configuration and verify the module version.

**Hints:**
- Use the `version` argument to specify the module version.
- Test the configuration with different module versions.

#### Optional Secondary Assignments
1. **Module Update:**
   - Update the module to a newer version and apply the changes.
   - Verify the impact of the module update on the configuration.

2. **Semantic Versioning:**
   - Implement semantic versioning in your module releases.
   - Test the behavior by applying the configuration with different version constraints.

3. **Module Dependency Management:**
   - Manage module dependencies using version constraints.
   - Test the configuration by applying it with various dependency versions.

### Section 5: Creating and Managing Composite Modules

#### Explanation
Composite modules in Terraform allow you to combine multiple modules into cohesive units, creating higher-level abstractions and reusability.

1. **Composite Modules:**
   - Combine multiple modules into a single composite module.
   - Pass inputs and outputs between the combined modules.

**Example Usage:**
```hcl
module "network" {
  source = "./modules/network"
  cidr   = "10.0.0.0/16"
}

module "compute" {
  source     = "./modules/compute"
  vpc_id     = module.network.vpc_id
  subnet_ids = module.network.subnet_ids
}
```

#### Primary Assignment
Create and manage composite modules in Terraform. Complete the following tasks:

1. Create multiple modules for different resources (e.g., network, compute).
2. Combine the modules into a single composite module.
3. Pass inputs and outputs between the combined modules.
4. Apply the configuration and verify the composite module functionality.

**Hints:**
- Create individual modules for different resource types.
- Combine the modules and manage their inputs and outputs.

#### Optional Secondary Assignments
1. **Nested Composite Modules:**
   - Create nested composite modules where one composite module includes another.
   - Verify the configuration by applying it and checking the resource properties.

2. **Advanced Composite Modules:**
   - Implement advanced logic and dependencies in composite modules.
   - Test the configuration with different input values.

3. **Reusable Composite Modules:**
   - Create reusable composite modules for common infrastructure patterns.
   - Use the reusable modules in multiple configurations and verify their flexibility.

#### Lab Summary
In this lab, you implemented dynamic blocks, utilized conditional expressions, managed multiple instances of resources, versioned and released modules, and created and managed composite modules in Terraform. These advanced techniques enhance the flexibility, reusability, and maintainability of your Terraform configurations. By completing the primary and optional assignments, you have gained hands-on experience with key Terraform concepts that will improve your ability to manage complex infrastructure as code effectively.

### More Information

For additional resources and detailed documentation to help you complete both the Primary and Optional Secondary Assignments, refer to the following links:

1. **Dynamic Blocks in Resource Configurations:**
   - [Dynamic Blocks](https://www.terraform.io/docs/language/expressions/dynamic-blocks.html)
   - [Terraform Configuration Language](https://www.terraform.io/docs/language/syntax/configuration.html)

2. **Conditional Expressions:**
   - [Conditional Expressions](https://www.terraform.io/docs/language/expressions/conditionals.html)
   - [Terraform Functions](https://www.terraform.io/docs/language/functions/index.html)

3. **Count and For_each:**
   - [Count and For_each](https://www.terraform.io/docs/language/meta-arguments/count.html)
   - [For_each Meta-Argument](https://www.terraform.io/docs/language/meta-arguments/for_each.html)

4. **Module Versioning:**
   - [Module Versioning](https://www.terraform.io/docs/language/modules/sources.html#version)
   - [Semantic Versioning](https://semver.org/)

5. **Creating and Managing Composite Modules:**
   - [Modules Overview](https://www.terraform.io/docs/language/modules/index.html)
   - [Creating Reusable Modules](https://www.terraform.io/docs/language/modules/develop/index.html)

These resources provide comprehensive information and examples that will assist you in successfully completing the lab assignments and deepening your understanding of dynamic blocks, conditional expressions, managing multiple instances, module versioning, and creating and managing composite modules.

